const tanggal = document.querySelector(".tanggal");

const filter = {
  tipedriver: "",
  date: "",
  waktujemput: "",
  capacity: "",
};

const backdrop = document.querySelector(".backdrop");

function changeTanggal() {
  const date = document.querySelector(".datepicker");
  tanggal.innerHTML = date.value;
  tanggal.value = date.value;
  const newTanggal = new Date(tanggal.value);
  filter.date = newTanggal.getTime();
}

function selectWaktu() {
  const waktu = document.querySelector(".select-waktu");
  filter.waktujemput = waktu.value.toString() * 3600;
}

function setCapacity() {
  const capacity = document.querySelector(".capacity");
  filter.capacity = parseInt(capacity.value);
}

const searchInput = document.getElementsByClassName("search-car");
for (i = 0; i < searchInput.length; i++) {
  searchInput[i].addEventListener("click", () => {
    backdrop.classList.add("showBackdrop");
  });
}
backdrop.addEventListener("click", () => {
  backdrop.classList.remove("showBackdrop");
});

//get value search
function isDriver() {
  const driver = document.querySelector(".driver");
  if (driver.value === "true") {
    filter.tipedriver = true;
  } else if (driver.value === "false") {
    filter.tipedriver = false;
  } else {
    filter.tipedriver = "";
  }
}

// function getTanggal(availableAt) {
//   const date = availableAt;
//   const yyyy = date.getFullYear();
//   let mm = date.getMonth() + 1; // Months start at 0!
//   let dd = date.getDate();

//   if (dd < 10) dd = "0" + dd;
//   if (mm < 10) mm = "0" + mm;

//   const tanggal = yyyy + "-" + mm + "-" + dd;
//   return tanggal;
// }

async function showcar() {
  backdrop.classList.remove("showBackdrop");
  const app = await Binar.listCars();
  const cardCar = document.querySelector(".cards");
  cardCar.innerHTML = "";
  // console.log(filter);

  for (i in app) {
    console.log(app[i]);
    // console.log(app[i].capacity);
    // console.log(filter.capacity);
    if (filter.tipedriver !== "") {
      if (app[i].withDriver !== filter.tipedriver) continue;
    }

    if (filter.date + filter.waktujemput < app[i].availableAt.getTime())
      continue;

    if (filter.capacity !== "") {
      if (filter.capacity !== app[i].capacity) continue;
    }

    cardCar.innerHTML += `<div class="card" style="width: 18rem">
          <img
            class="card-img-top"
            src="${app[i].image}"
            alt="Card image cap"
          />
          <div class="card-body">
            <h5 class="card-title">${app[i].manufacture} ${app[i].model}</h5>
            <p class="card-text rentperday">Rp. ${app[i].rentPerDay} / hari</p>
            <p class="card-text">${app[i].description}</p>
            <p class="card-text"><img style="width: 20px; height: 20px;" src="./images/users.png" alt=""
            /> ${app[i].capacity} orang</p>
            <p class="card-text"><img style="width: 20px; height: 20px;" src="./images/settings.png" alt=""
            /> ${app[i].transmission}</p>
            <p class="card-text"><img style="width: 20px; height: 20px;" src="./images/fi_calendar.svg" alt=""
            /> ${app[i].year}</p>

            <a href="#" class="btn btn-success btn-card">Pilih Mobil</a>
          </div>
        </div>`;
  }

  // app.forEach((car) => {
  //   // console.log(car.availableAt.getTime());
  //   console.log(car);

  //   if (car.withDriver !== filter.tipedriver) console.log("");

  //   cardCar.innerHTML += `<div class="card" style="width: 18rem">
  //         <img
  //           class="card-img-top"
  //           src="${car.image}"
  //           alt="Card image cap"
  //         />
  //         <div class="card-body">
  //           <h5 class="card-title">${car.manufacture} ${car.model}</h5>
  //           <p class="card-text">${car.rentPerDay}</p>
  //           <p class="card-text">${car.description}</p>
  //           <p class="card-text">${car.capacity}</p>
  //           <p class="card-text">${car.year}</p>

  //           <a href="#" class="btn btn-warning">Sewa</a>
  //         </div>
  //       </div>`;
  // });
}
