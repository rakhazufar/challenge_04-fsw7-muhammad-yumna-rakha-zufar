const http = require("http");
const fs = require("fs");

function loadHTML(file, statusCode, res) {
  let contentType = "text/html";
  let ekstensi = file.split(".");
  ekstensi = ekstensi[ekstensi.length - 1];
  switch (ekstensi) {
    case "js":
      contentType = "application/javascript";
      break;
    case "css":
      contentType = "text/css";
      break;
    case "png":
      contentType = "image/png";
      break;
    case "jpg":
    case "jpeg":
      contentType = "image/jpg";
      break;
    case "svg":
      contentType = "image/svg+xml";
      break;
  }
  res.writeHead(statusCode, { "content-type": contentType });
  let html = fs.readFileSync(file);
  res.end(html);
}

function onRequest(req, res) {
  switch (req.url) {
    case "/":
      loadHTML("public/index.html", 200, res);
      break;
    case "/searchCar.html":
      loadHTML("public/searchCar.html", 200, res);
      break;
    default:
      const asset = "public" + req.url;
      if (fs.existsSync(asset)) {
        loadHTML(asset, 200, res);
      } else {
        loadHTML("public/404.html", 404, res);
      }
      break;
  }
  //   res.writeHead(200, { "content-type": "text/html" });

  //   let html = fs.readFileSync("../scripts/index.html");
  //   res.end(html);
}

const server = http.createServer(onRequest);

server.listen(8000);
